import java.util.*;
import java.io.*;


/**
 * Cette classe permet de calculer et d'afficher diff\u00e9rentes informations concernant des trajets de train
 * @author THEBAULT Claire
 */
class Planification {

    // Variables globales accessibles par toutes les m\u00e9thodes
    /**
     * Array list contenant les trajets
     */
    ArrayList<String> trajets = new ArrayList<String>();
    /**
     * Array list contenant les horaires
     */
    ArrayList<Integer> horaires = new ArrayList<Integer>();


    /**
     * Le point d'entr\u00e9 du programme
     */
    void principal() {
        System.out.println();
        
        testRemplirLesCollections();
        testAfficherHorairesEtDureeTrajets2Gares();
        testChercherCorrespondances();
        testObtenirInfosUnTrajet();   
        testObtenirInfosUnHoraire();
        testTrouverTousLesTrajets();
    }
    

    /*** Les m\u00e9thodes de test ***/

    /**
     * Test de la m\u00e9thode remplirLesCollections
     */
    void testRemplirLesCollections() {
        System.out .println("*** testRemplirLesCollections***");

        // Appel de la m\u00e9thode \u00e0 tester "cas normal"
        remplirLesCollections ( "./TrajetsEtHoraires.txt" );
        // Test VISUEL de la bonne lecture du fichier

        afficherHorairesEtDureeTousTrajets();
        trajets.clear();//Permet de ne pas afficher en double
        horaires.clear();
        System.out.println();
    }


    /**
     * Test de la m\u00e9thode afficherHorairesEtDureeTrajets2Gares
     */
    void testAfficherHorairesEtDureeTrajets2Gares() {
        System.out .println("*** testAfficherHorairesEtDureeTrajets2Gares***");

        // D’abord remplir les collections
        remplirLesCollections ( "./TrajetsEtHoraires.txt" );
        // Appel de la m\u00e9thode \u00e0 tester "cas normal"
        // Test VISUEL
        afficherHorairesEtDureeTrajets2Gares ( "Vannes", "Redon" );
        trajets.clear();
        horaires.clear();
        System.out.println();
    }


    /**
     * Test de la m\u00e9thode chercherCorrespondances
     */
    void testChercherCorrespondances(){
        System.out .println("*** testChercherCorrespondances***");

        // D’abord remplir les collections
        remplirLesCollections ( "./TrajetsEtHoraires.txt" );

        Duree heure1 = new Duree(20, 0, 0);
        Duree heure2 = new Duree(8, 0, 0);
        String gare = "Vannes";
        ArrayList<String> correspondance = chercherCorrespondances(gare, heure1);
        ArrayList<String> correspondance2 = chercherCorrespondances(gare, heure2);
        String heureDepart1 = heure1.enTexte('H');
        String heureDepart2 = heure2.enTexte('H');

        testCasChercherCorrespondances(correspondance, gare, heureDepart1);
        testCasChercherCorrespondances(correspondance2, gare, heureDepart2);
        
        trajets.clear();
        horaires.clear();
        System.out.println();
        System.out.println();
    }
    
    void testCasChercherCorrespondances(ArrayList<String> correspondance, String gare, String heureDepart){
        if (correspondance == null){
            System.out.println("Il n'existe pas de correspondance partant de la gare de " + gare + " apr\u00e8s " + heureDepart);
        }
        else {
            System.out.println("La gare de d\u00e9part est " + gare + " \u00e0 " + 
            heureDepart + " et les id des trajets possibles sont :" +
            correspondance);
        }
    }

    
    /**
     * Test de la m\u00e9thode obtenirInfosUnTrajet
     */
    void testObtenirInfosUnTrajet(){
        System.out .println("*** testObtenirInfosUnTrajet***");

        // D’abord remplir les collections
        remplirLesCollections ( "./TrajetsEtHoraires.txt" );

        String idTrajet = "1";
        
        String[] info = obtenirInfosUnTrajet("1");
        System.out.println("Trajet d'identifiant : " + idTrajet );

        System.out.println("D\u00e9part en " + info[0] + " de la gare de " + info[1] +", arriv\u00e9e \u00e0 la gare de " + info[2] );
        trajets.clear();
        horaires.clear();
        System.out.println();
        System.out.println();
    }
    

    /**
     * Test de la m\u00e9thode obtenirInfosUnHoraire
     */
    void testObtenirInfosUnHoraire(){
        System.out .println("*** testObtenirInfosUnHoraire***");

        // D’abord remplir les collections
        remplirLesCollections ( "./TrajetsEtHoraires.txt" );

        String idTrajet = "1";
        int[] info = obtenirInfosUnHoraire(idTrajet);

        System.out.println("Trajet d'identifiant : " + idTrajet );

        Duree depart = new Duree(info[0], info[1], 0);
        Duree arrivee = new Duree(info[2], info[3], 0);

        System.out.println("\tD\u00e9part \u00e0 " + depart.enTexte('H'));
        System.out.println("\tArriv\u00e9e \u00e0 " + arrivee.enTexte('H'));
        trajets.clear();
        horaires.clear();
        System.out.println();
        System.out.println();
    }


    /**
     * Test de la m\u00e9thode trouverTousLesTrajets
     */
    void testTrouverTousLesTrajets(){
        System.out .println("*** testTrouverTousLesTrajets***");

        // D’abord remplir les collections
        remplirLesCollections ( "./TrajetsEtHoraires.txt" );

        String gareDep = "Vannes";
        ArrayList<String> trajetTrouve = trouverTousLesTrajets(gareDep);
        System.out.println("Identifiants des trajets partant de la gare de " + gareDep +" : ");
        for (int i = 0; i < trajetTrouve.size(); i++){
            System.out.println(trajetTrouve.get(i));
        }
        trajets.clear();
        horaires.clear();
        System.out.println();
    }
    
    

    /*** Le code des m\u00e9thodes demand\u00e9es ***/

    /**
     * Lit le fichier contenant les horaires de trains et les informations sur les gares et les trains et rempli deux listes avec
     * @param nomFich nom du fichier \u00e0 acc\u00e9der pour obtenir les informations de tous les trajets
     */
    void remplirLesCollections (String nomFich ) {
        boolean eof = false; // Pour voir quand on est arrive a la fin du fichier a parcourir
        String str;
        BufferedReader tampon; 

        try {
            tampon = new BufferedReader ( new FileReader ( nomFich ) );
            while ( ! eof ) {
                str = tampon.readLine();
                if ( str == null ) { 
                    eof = true; 
                }
                else {
                    String[] lesInfos = str.split("/");
                    int splitLength = lesInfos.length;
                    for (int i = 0; i < lesInfos.length; i++) {
                        String sansBlanc = lesInfos[i].trim();
                        if (splitLength == 4) { // Il y a 4 informations par ligne pour les trajets, donc si une ligne contient 4 informations on la range dans trajets
                            trajets.add(sansBlanc);
                        }
                        if (splitLength == 5) {
                            horaires.add(Integer.parseInt(sansBlanc));
                        }
                    }
                }
            }
            tampon.close();
        }
        catch (FileNotFoundException e) {
            System.out.println ( e.getMessage() );
        }
        catch (IOException e) {
            System.out.println ( e.getMessage() );
        }
    }


    /**
     * M\u00e9thode permettant d'afficher tous les trajets qui existent
     */
    void afficherHorairesEtDureeTousTrajets() {
        int tabTrajSize = trajets.size();

        if (tabTrajSize % 4 == 0) {//on verifie qu'il n y a pas d'erreur dans l'arraylist contenant les trajet
            int tabHorairesSize = horaires.size();

            if (tabHorairesSize % 5 == 0) {//de meme avec les horaires
                for (int i = 0; i < tabTrajSize; i = i + 4) {
                    String stringTrajets = trajets.get(i);
                    System.out.println("Train " + trajets.get(i + 1) + " num\u00e9ro " + trajets.get(i) + " :");
                    int intTrajets = Integer.parseInt(stringTrajets);

                    for (int j = 0; j < tabHorairesSize; j = j + 5) {
                        int intHoraires = horaires.get(j);
                        if (intTrajets == intHoraires) {
                            int intDepartHeure = horaires.get(j + 1);
                            int intDepartMin = horaires.get(j + 2);
                            int intArriveeHeure = horaires.get(j + 3);
                            int intArriveeMin = horaires.get(j + 4);

                            Duree depart = new Duree(intDepartHeure, intDepartMin, 0);
                            Duree arrivee = new Duree(intArriveeHeure, intArriveeMin, 0);

                            System.out.println("\tD\u00e9part de " + trajets.get(i + 2) + " \u00e0 " + depart.enTexte('H'));
                            System.out.println("\tArriv\u00e9e \u00e0 " + trajets.get(i + 3) + " \u00e0 " + arrivee.enTexte('H'));
                            arrivee.soustraire(depart);
                            System.out.println("\tDur\u00e9e du trajet - " + arrivee.enTexte('H') + "\n");

                        }
                    }
                }
            }
            else System.out.println("erreur dans les donn\u00e9s de horaires");
        }
        else System.out.println("erreur dans les donn\u00e9s de trajets");
    }

    /**
     * Permet d'afficher tous les trajets au d\u00e9part d'une gare choisie et qui arrive \u00e0 une autre gare donn\u00e9e
     * @param gareDep nom de la gare de d\u00e9part
     * @param gareDest nom de la gare d'arriv\u00e9e
     */
    void afficherHorairesEtDureeTrajets2Gares ( String gareDep, String gareDest ) {
        int tabTrajSize = trajets.size();
        if (tabTrajSize % 4 == 0) {
            int tabHorairesSize = horaires.size();
            if (tabHorairesSize % 5 == 0) {
                for (int i = 0; i < tabTrajSize; i = i + 4) {
                    if (trajets.get(i + 2).equals(gareDep) && trajets.get(i + 3).equals(gareDest)) {// on affiche seulement si la gare de depart est bien la meme et la gare d'arrivee aussi
                        String stringTrajets = trajets.get(i);
                        System.out.println("Train " + trajets.get(i + 1) + " num\u00e9ro " + trajets.get(i) + " :");
                        int intTrajets = Integer.parseInt(stringTrajets);

                        for (int j = 0; j < tabHorairesSize; j = j + 5) {
                            int intHoraires = horaires.get(j);
                            if (intTrajets == intHoraires) {
                                int intDepartHeure = horaires.get(j + 1);
                                int intDepartMin = horaires.get(j + 2);
                                int intArriveeHeure = horaires.get(j + 3);
                                int intArriveeMin = horaires.get(j + 4);

                                Duree depart = new Duree(intDepartHeure, intDepartMin, 0);
                                Duree arrivee = new Duree(intArriveeHeure, intArriveeMin, 0);

                                System.out.println("\tD\u00e9part de " + trajets.get(i + 2) + " \u00e0 " + depart.enTexte('H'));
                                System.out.println("\tArriv\u00e9e \u00e0 " + trajets.get(i + 3) + " \u00e0 " + arrivee.enTexte('H'));
                                arrivee.soustraire(depart);
                                System.out.println("\tDur\u00e9e du trajet - " + arrivee.enTexte('H') + "\n");

                            }
                        }
                    }
                }
            }
            else System.out.println("erreur dans les donn\u00e9s de 'horaires'");
        }
        else System.out.println("erreur dans les donn\u00e9s de 'trajets'");
    }

    
    /**
     * Cherche toute les correspondances partant d'une gare \u00e0 partir d'une certaine heure
     * @param gare nom de la gare de d\u00e9part
     * @param heure heure apr\u00e8s laquelle on cherche une correspondance
     * @return liste de tous les trajets correspondant \u00e0 la recherche
     */
    ArrayList<String> chercherCorrespondances ( String gare, Duree heure ) {

        ArrayList<String> bonTrajet = new ArrayList<String>();
        ArrayList<String> idTrajets = new ArrayList<String>();
        
        idTrajets = trouverTousLesTrajets(gare);
        boolean check = false;

        if (idTrajets != null) {//S'il existe des trajets au depart de la gare de depart saisie on rentre dans la boucle
            for (int i = 0; i < idTrajets.size(); i++) {
            
                int[] horaireInfos = new int[4];
                horaireInfos = obtenirInfosUnHoraire(idTrajets.get(i));
                Duree heureDepart = new Duree(horaireInfos[0], horaireInfos[1], 0);
    
                if (heureDepart.compareA(heure) == 1) {//On verifie que les trajets au depart de la gare voulue sont pus tard que l'heure souhaitee
                    bonTrajet.add(idTrajets.get(i));
                    check = true;
                }
            }
            if (!check) {// Si aucun des trajets n'etait apres l'heure souhaitee on renvoie qu'il n'existait aucun trajet correspondant a la description
                bonTrajet = null;
            }
        }
        return bonTrajet;
    }


    /**
     * Permet d'obtenir les informations sur les gares et le type de train d'un trajet donn\u00e9 \u00e0 partir de son identifiant
     * @param idTrajet identifiant du trajet cherch\u00e9
     * @return une liste contenant le type de train, la gare de d\u00e9part et la gare d'arriv\u00e9e du trajet recherch\u00e9
     */
    String[] obtenirInfosUnTrajet ( String idTrajet ) {
        String[] ret = new String[3];

        if (idTrajet == null || trajets == null) {
            ret = null;
        }
        else {
            boolean check = false;
            int i = 0;
            int tabTrajSize = trajets.size();

            while (i < tabTrajSize && !check) {

                if (trajets.get(i).equals(idTrajet)) {

                    ret[0] = trajets.get(i + 1);
                    ret[1] = trajets.get(i + 2);
                    ret[2] = trajets.get(i + 3);
                    check = true;

                }
                i = i + 4;
            }
            if (!check) {
                ret = null;
            }
        }
        return ret;
    }


    /**
     * Permet d'obtenir les informations sur les horaires d'un trajet donn\u00e9 \u00e0 partir de son identifiant
     * @param idTrajet identifiant du trajet cherch\u00e9
     * @return une liste contenant l'heure de d\u00e9part, les minutes \u00e0 l'heure dde d\u00e9part, les minutes \u00e0 l'arriv\u00e9e
     */
    int[] obtenirInfosUnHoraire ( String idTrajet ) {
        int[] ret = new int[4];

        if (idTrajet == null || horaires == null) {
            ret = null;
        }

        else {
            boolean check = false;
            int i = 0;
            int tabHorairesSize = horaires.size();

            while (i < tabHorairesSize && !check) {

                if (horaires.get(i) == Integer.parseInt(idTrajet)) {//Si l'identifiant du trajet dont on veut obtenir les informations est le meme que un trajet contenu dans l'array list horaire, on stock son contenu dans ret  

                    ret[0] = horaires.get(i + 1);
                    ret[1] = horaires.get(i + 2);
                    ret[2] = horaires.get(i + 3);
                    ret[3] = horaires.get(i + 4);
                    check = true;

                }
                i = i + 5;
            }

            if (!check) {
                ret = null;
            }

        }
        return ret;
    }


    /**
     * Permet de trouver tous les trajets au d\u00e9part d'une gare, peu importe les horaires des trains
     * @param gareDep gare de d\u00e9part
     * @return la liste des identifiants des trajets au d\u00e9part de la gare recherch\u00e9e
     */
    ArrayList<String> trouverTousLesTrajets ( String gareDep ) {
        ArrayList<String> trajetsGareDep = new ArrayList<String>();

        if (gareDep == null || trajets == null) {
            trajetsGareDep = null;
        }

        else {
            int i = 0;
            int trajetsSize = trajets.size();
            boolean check = false;

            while (i < trajetsSize) {

                if (trajets.get(i + 2).equals(gareDep)) {
                    trajetsGareDep.add(trajets.get(i));
                    check = true;
                }
                i = i + 4;
                
            }
            if (!check) {
                trajetsGareDep = null;
            }
        }
        return trajetsGareDep;
    }
}