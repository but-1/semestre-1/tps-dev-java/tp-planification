/**
 * Cette classe sert \u00e0 v\u00e9rifier les m\u00e9thodes de la classe Duree
 * @author THEBAULT Claire
 */
class TestDuree {

    /**
     * Point d'entr\u00e9e du programme
     */
    void principal(){
        testConstructeur1EtGetLeTemps();
        testConstructeur2EtGetLeTemps();
        testAjouter();
        testSoustraire();
        testCompareA();
        testEnTexte();

    }

    /**
     * test un appel de la m\u00e9thode constructeur1EtGetLeTemps
     * @param millisec temps en milli secondes
     * @param res r\u00e9sultat attendu
     * @param casErreur si la dur\u00e9e rentr\u00e9e est n\u00e9gative renvoir un message d'erreur
     */
    void testCasConstructeur1EtGetLeTemps(int millisec, int res, boolean casErreur){
        if(!casErreur){
            Duree d1 = new Duree(millisec);
            if(d1.getLeTemps()==res){
                System.out.println("Test r\u00e9ussi");
            }
            else {
                System.out.println("Echec");
            }
        }
        else {
            System.out.println("Message erreur attendu car duree incorrecte");
        }
        System.out.println();
    }

    /**
     * teste de la m\u00e9thode constructeur1EtGetLeTemps
     */
    void testConstructeur1EtGetLeTemps(){
        System.out.println("*** testConstructeur1EtGetLeTemps ***");
        System.out.println("** Cas normaux **");
        testCasConstructeur1EtGetLeTemps(1000, 1000, false);

        System.out.println("** Cas erreurs **");
        testCasConstructeur1EtGetLeTemps(-15, 0, true);
        System.out.println();
    }

    /**
     * test un appel de la m\u00e9thode constructeur2EtGetLeTemps
     * @param heure temps en heure
     * @param min temps en minute
     * @param sec temps en seconde
     * @param res r\u00e9sulat attendu
     * @param casErreur si la dur\u00e9e rentr\u00e9e est n\u00e9gative renvoir un message d'erreur
     */
    void testCasConstructeur2EtGetLeTemps(int heure, int min, int sec, int res, boolean casErreur){
        if(!casErreur){
            Duree d1 = new Duree(heure, min, sec);
            if(d1.getLeTemps()==res){
                System.out.println("Test r\u00e9ussi");
            }
            else {
                System.out.println("Echec");
            }
        }
        else {
            System.out.println("Message erreur attendu car duree incorrecte");
            //d1 = new Duree(millisec);
        }
        System.out.println();
    }

    /**
     * teste de la m\u00e9thode constructeur2EtGetLeTemps
     */
    void testConstructeur2EtGetLeTemps(){
        System.out.println("*** testConstructeur2EtGetLeTemps ***");
        System.out.println("** Cas normaux **");
        testCasConstructeur2EtGetLeTemps(1, 1, 0, 3660000, false);

        System.out.println("** Cas erreurs **");
        testCasConstructeur2EtGetLeTemps(-15,0, 0, 0, true);
        System.out.println();
    }
    
    /**
     * test un appel de la m\u00e9thode ajouter
     * @param d1 dur\u00e9e dont on doit ajouter la deuxi\u00e8me dur\u00e9e
     * @param d2 dur\u00e9e \u00e0 additionner
     * @param res r\u00e9sulat attendu
     */
    void  testCasAjouter(Duree d1, Duree d2, int res){
        d1.ajouter(d2);
        if (d1.getLeTemps()==res){
            System.out.println("Test r\u00e9ussi");
        }
        else {
            System.out.println("Echec");
        }
        System.out.println();
    }


    /**
     * teste de la m\u00e9thode ajouter
     */
    void testAjouter(){
        System.out.println("*** testAjouter ***");

        Duree d1 = new Duree(1000);
        Duree d2 = new Duree(2000);

        System.out.println("** Cas normaux **");
        testCasAjouter(d1, d2, 3000);
        System.out.println();
    }

    
    /**
     * test un appel de la m\u00e9thode soustraire
     * @param d1 dur\u00e9e dont on doit soustraire la deuxi\u00e8me dur\u00e9e
     * @param d2 dur\u00e9e \u00e0 soustraire
     * @param res r\u00e9sulat attendu
     * @param casErreur si la dur\u00e9e rentr\u00e9e est n\u00e9gative renvoir un message d'erreur
     */
    void  testCasSoustraire(Duree d1, Duree d2, int res, boolean casErreur){
         if(!casErreur){
            d1.soustraire(d2);
            if (d1.getLeTemps()==res){
                System.out.println("Test r\u00e9ussi");
            }
            else {
                System.out.println("Echec");
            }
            System.out.println();
        }
        else {
            System.out.println("Message erreur attendu car dur\u00e9e finale n\u00e9gative");
            
        }
    }

    /**
     * teste de la m\u00e9thode soustraire
     */
    void testSoustraire(){
        System.out.println("*** testSoustraire ***");

        Duree d1 = new Duree(1000);
        Duree d2 = new Duree(2000);

        System.out.println("** Cas normaux **");
        testCasSoustraire(d2, d1, 1000, false);

        System.out.println("** Cas erreurs **");
        testCasSoustraire(d1, d2, -1000, true);

        System.out.println();
    }
    
    /**
     * test un appel de la m\u00e9thode compareA
     * @param d1 premi\u00e8re dur\u00e9e \u00e0 comparer
     * @param d2 deuxi\u00e8me dur\u00e9e \u00e0 comparer
     * @param res r\u00e9sulat attendu
     */
    void  testCasCompareA(Duree d1, Duree d2, int res){
        if (d1.compareA(d2)==res){
            System.out.println("Test r\u00e9ussi");
        }
        else {
            System.out.println("Echec");
        }
        System.out.println();
    }

    /**
     * teste de la m\u00e9thode compareA
     */
    void testCompareA(){
        System.out.println("*** testCompareA ***");

        Duree d1 = new Duree(1000);
        Duree d2 = new Duree(2000);
        Duree d3 = null;

        System.out.println("** Cas normaux **");
        testCasCompareA(d1, d1, 0);
        testCasCompareA(d1, d2, -1);
        testCasCompareA(d2, d1, 1);
        testCasCompareA(d1, d3, -2);
        System.out.println();
    }


    /**
     * test un appel de la m\u00e9thode enTexte
     * @param st1 affichage d'une dur\u00e9e \u00e0 comparer
     * @param res r\u00e9sulat attendu
     */
    void  testCasEnTexte(String st1, String res){
        if (res.equals(st1)){
            System.out.println("Test r\u00e9ussi");
        }
        else {
            System.out.println("Echec");
        }
        System.out.println();
    }


    /**
     * teste de la m\u00e9thode enTexte
     */
    void testEnTexte(){
        System.out.println("*** testEnTexte ***");
        Duree d1 =new Duree(3, 4, 40);
        String st1 = "03:04:40";
        String st2 = d1.enTexte('H');

        Duree d2 =new Duree(25, 0, 0);
        String st3 = "01 jours 01 h";
        String st4 = d2.enTexte('J');

        System.out.println("** Cas normaux **");
        testCasEnTexte(st1, st2);

        testCasEnTexte(st3, st4);
        System.out.println();
    }

}
