// Source code is decompiled from a .class file using FernFlower decompiler.
public class Duree {
   private int leTemps;

   public Duree(int var1) {
      if (var1 < 0) {
         System.out.println("Erreur : le temps est n\u00e9gatif");
      } else {
         this.leTemps = var1;
      }

   }

   public Duree(int var1, int var2, int var3) {
      if (var1 >= 0 && var2 >= 0 && var3 >= 0) {
         this.leTemps = (3600 * var1 + 60 * var2 + var3) * 1000;
      } else {
         System.out.println("Erreur : au moins un des temps parmi heures, minutes, secondes est n\u00e9gatif");
      }

   }

   public int getLeTemps() {
      return this.leTemps;
   }

   public int compareA(Duree var1) {
      byte var2;
      if (var1 != null) {
         if (var1.leTemps < 0) {
            System.out.println("Erreur : la dur\u00e9e compar\u00e9e est n\u00e9gative");
            var2 = -2;
         } else if (this.leTemps < var1.getLeTemps()) {
            var2 = -1;
         } else if (this.leTemps == var1.getLeTemps()) {
            var2 = 0;
         } else {
            var2 = 1;
         }
      } else {
         System.out.println("Erreur : autreDuree est null");
         var2 = -2;
      }

      return var2;
   }

   public String enTexte(char var1) {
      String var2 = null;
      int[] var3 = this.enJHMS();
      String var4;
      String var5;
      switch (var1) {
         case 'H':
            if (var3[0] * 24 + var3[1] < 10) {
               var4 = "0" + String.valueOf(var3[0] * 24 + var3[1]);
            } else {
               var4 = String.valueOf(var3[0] * 24 + var3[1]);
            }

            if (var3[2] < 10) {
               var5 = "0" + String.valueOf(var3[2]);
            } else {
               var5 = String.valueOf(var3[2]);
            }

            String var6;
            if (var3[3] < 10) {
               var6 = "0" + String.valueOf(var3[3]);
            } else {
               var6 = String.valueOf(var3[3]);
            }

            var2 = var4 + ":" + var5 + ":" + var6;
            break;
         case 'J':
            if (var3[0] < 10) {
               var4 = "0" + String.valueOf(var3[0]);
            } else {
               var4 = String.valueOf(var3[0]);
            }

            if (var3[1] < 10) {
               var5 = "0" + String.valueOf(var3[1]);
            } else {
               var5 = String.valueOf(var3[1]);
            }

            var2 = var4 + " jours " + var5 + " h";
            break;
         case 'M':
            var2 = new String("" + this.leTemps + " millisec");
            break;
         case 'S':
            var2 = new String(this.leTemps / 1000 + " sec");
            break;
         default:
            var2 = new String("Erreur - " + var1 + " : format pas accept\u00e9 !");
      }

      return var2;
   }

   public void ajouter(Duree var1) {
      if (var1 != null) {
         if (var1.leTemps < 0) {
            System.out.println("Erreur : la dur\u00e9e ajout\u00e9e est n\u00e9gative");
         } else {
            this.leTemps += var1.getLeTemps();
         }
      } else {
         System.out.println("Erreur : autreDuree est null");
      }

   }

   public void soustraire(Duree var1) {
      if (var1 != null) {
         if (var1.leTemps < 0) {
            System.out.println("Erreur : la dur\u00e9e soustraite est n\u00e9gative");
         } else {
            int var2 = this.leTemps;
            this.leTemps -= var1.getLeTemps();
            if (this.leTemps < 0) {
               System.out.println("Erreur : la dur\u00e9e apr\u00e8s soustraction devient n\u00e9gative");
               this.leTemps = var2;
            }
         }
      } else {
         System.out.println("Erreur : autreDuree est null");
      }

   }

   private int[] enJHMS() {
      int[] var1 = new int[5];
      var1[0] = this.leTemps / 86400000;
      int var2 = this.leTemps % 86400000;
      var1[1] = var2 / 3600000;
      var2 %= 3600000;
      var1[2] = var2 / '\uea60';
      var2 %= 60000;
      var1[3] = var2 / 1000;
      var1[4] = var2 % 1000;
      return var1;
   }
}
